//
//  FaceIdSignInViewController.m
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 11/28/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "FaceIdSignInViewController.h"
#import <CoreImage/CoreImage.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import "ViewController.h"

@interface FaceIdSignInViewController ()

@property (nonatomic, strong) LAContext *context;

@end

@implementation FaceIdSignInViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.context = [[LAContext alloc] init];
}

#pragma mark - Actions

- (IBAction)loginWithTouchIdButtonAction:(id)sender {
    NSError *error;
    
    BOOL canAuthentication = [self.context canEvaluatePolicy:LAPolicyDeviceOwnerAuthentication error:&error];
    if (canAuthentication) {
        [self.context evaluatePolicy:LAPolicyDeviceOwnerAuthentication localizedReason:@"Test FaceID" reply:^(BOOL success, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (success) {
                    ViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ViewController class])];
                    [self.navigationController pushViewController:vc animated:YES];
                } else {
                    NSLog(@"%@",error);
                }
            });
        }];
    }
}

@end
