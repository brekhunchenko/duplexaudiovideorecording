//
//  extern.h
//  DuplexAudioVideoRecording
//
//  Created by Yushen Han on 11/12/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#ifndef extern_h
#define extern_h

typedef enum    {
    kMMAudioSupportWirelessPlayback_None = 0,
    kMMAudioSupportWirelessPlayback_W1,
    kMMAudioSupportWirelessPlayback_A2DP,
    kMMAudioSupportWirelessPlayback_HFP,
    kMMAudioSupportWirelessPlayback_AirPlay
} kMMAudioSupportWirelessPlaybackStatus;

extern kMMAudioSupportWirelessPlaybackStatus g_wireless_playback_status;

extern int g_channels;
extern float g_sine_frequency;
extern float g_sample_rate;
extern float g_output_tap_duration;
extern unsigned g_input_buffer_length;
extern unsigned g_output_buffer_length;
extern unsigned g_input_counter;
extern unsigned g_output_counter;

extern float g_max_off_sync;    // test statistics


#endif /* extern_h */
