//
//  VideoPlayerViewController.h
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 10/29/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoPlayerViewController : UIViewController

@property (strong, nonatomic) NSURL *videoUrl;
@property (strong, nonatomic) NSURL* audioUrl;

@end

NS_ASSUME_NONNULL_END
