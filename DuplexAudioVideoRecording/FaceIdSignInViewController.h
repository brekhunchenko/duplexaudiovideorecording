//
//  FaceIdSignInViewController.h
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 11/28/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FaceIdSignInViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
