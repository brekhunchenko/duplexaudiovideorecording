//
//  common.h
//  DuplexAudioVideoRecording
//
//  Created by Yushen Han on 11/12/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#ifndef common_h
#define common_h


//git config --get remote.origin.url
//git remote set-url origin https://{new url with username replaced}
//https://<username>:<password>@bitbucket.org/<username>/<repo>.git
// git remote set-url origin 'https://yushenhan@bitbucket.org/brekhunchenko/duplexaudiovideorecording.git/'

#include "extern.h"

//typedef enum    {
//    kMMAudioSupportWirelessPlayback_None = 0,
//    kMMAudioSupportWirelessPlayback_W1,
//    kMMAudioSupportWirelessPlayback_A2DP,
//    kMMAudioSupportWirelessPlayback_HFP,
//    kMMAudioSupportWirelessPlayback_AirPlay
//} kMMAudioSupportWirelessPlaybackStatus;

kMMAudioSupportWirelessPlaybackStatus g_wireless_playback_status = 0;

int g_channels = 1;
float g_sine_frequency = 220.0f;
float g_sample_rate = 48000.0f;
float g_output_tap_duration =  1024.0f / (float) (48000.0f); //  0.1706667; // 100ms - 400ms    // here need to change according to g_sample_rate
unsigned g_input_buffer_length = 1024;
unsigned g_output_buffer_length = 1024; // output_buffer_length or output_tap_duration must match
unsigned g_input_counter = 0;
unsigned g_output_counter = 0;

float g_max_off_sync = 0.0;

#endif /* common_h */
