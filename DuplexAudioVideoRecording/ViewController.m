//
//  ViewController.m
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 10/29/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ViewController.h"
#import "Video Player/VideoPlayerViewController.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - ViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _setupAudioFile];
}

#pragma mark - Setup

- (void)_setupAudioFile {
    NSURL* url = [[NSBundle mainBundle] URLForResource:@"rachmaninoff_vocalise" withExtension:@"wav"];
    self.audioFileURL = url;
}

#pragma mark - Actions

- (IBAction)recordButtonAction:(UIButton *)sender {
    if (self.isRecording) {
        [self stopRecording];
    } else {
        NSURL *videoOutputURL = [[[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:@"test1"] URLByAppendingPathExtension:@"mov"];
        
        NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentPath = [searchPaths objectAtIndex:0];
        NSString*audioFilePath = [documentPath stringByAppendingPathComponent:@"temp.wav"];
//        NSString*audioFilePath = @"/Users/brekhunchenko/Desktop/test.wav";
        NSURL* audioOutputURL = [NSURL fileURLWithPath:audioFilePath];
        [self startRecordingWithVideoOutputUrl:videoOutputURL
                                audioOutputUrl:audioOutputURL
                                     didRecord:^(NSURL * _Nonnull outputFileUrl, NSError * _Nonnull error) {
            VideoPlayerViewController *vc = [[VideoPlayerViewController alloc] init];
            vc.videoUrl = outputFileUrl;
            vc.audioUrl = audioOutputURL;
            [self.navigationController pushViewController:vc animated:YES];
        }];
    }
    
    [sender setSelected:!sender.isSelected];
}

@end
