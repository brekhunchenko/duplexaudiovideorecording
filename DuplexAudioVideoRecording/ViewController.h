//
//  ViewController.h
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 10/29/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDuplexVideoAudioCameraController.h"

@interface ViewController : MMDuplexVideoAudioCameraController

@end

