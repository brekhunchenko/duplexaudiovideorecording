//
//  MMDuplexVideoAudioCameraController.m
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 10/29/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMDuplexVideoAudioCameraController.h"
#import <AVFoundation/AVFoundation.h>
#import "MMAudioUnit/MMProcessingAudioUnit.h"
#import "MMAudioUnit/MMAudioGeneratorUnit.h"
#import <Accelerate/Accelerate.h>
#include "extern.h"

Float32 fill_sine(Float32 *out_data, const UInt32 length, const Float64 start_phase, const Float64 phase_per_frame) {
    if (!out_data || length == 0) {
        return start_phase;
    }
    
    Float64 phase = start_phase;
    
    for (UInt32 i = 0; i < length; ++i) {
        out_data[i] = phase;
        phase = fmod(phase + phase_per_frame, 2.0 * M_PI);
    }
    
    const int len = length;
    vvsinf(out_data, out_data, &len);
    
    for (UInt32 i = 0; i < length; ++i) {
        out_data[i] *= 0.2; // otherwise clipping
    }
    
    return phase;
}

@interface MMDuplexVideoAudioCameraController () < AVCaptureFileOutputRecordingDelegate >

@property (nonatomic, assign, readwrite) BOOL isRecording;
@property (nonatomic, copy) void (^didRecordCompletionBlock)(NSURL *outputFileUrl, NSError *error);

/* Video Recording */
@property (strong, nonatomic) UIView *preview;
@property (strong, nonatomic) AVCaptureSession *session;
@property (strong, nonatomic) AVCaptureDevice *videoCaptureDevice;
@property (strong, nonatomic) AVCaptureDevice *audioCaptureDevice;
@property (strong, nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (strong, nonatomic) AVCaptureMovieFileOutput *movieFileOutput;

/* Audio Recording */
@property (strong, nonatomic) AVAudioEngine* audioEngine;
@property (strong, nonatomic) AVAudioFile* audioFile;
@property (strong, nonatomic) AVAudioPlayerNode* audioPlayer;
@property (strong, nonatomic) AVAudioPlayerNode* audioFilePlayer;
@property (strong, nonatomic) AVAudioMixerNode* mixer;
@property (strong, nonatomic) AVAudioMixerNode* finalMixer;

@end

@implementation MMDuplexVideoAudioCameraController {
    ExtAudioFileRef outref;
}

- (NSError*) _setAVAudioSessionCategoryPlayAndRecordAccordingToWirelessStatus {
    NSError* error = nil;
    switch ( g_wireless_playback_status ) {
        case kMMAudioSupportWirelessPlayback_None:
            g_sample_rate = 48000.0f;
            g_output_tap_duration =  (float) g_output_buffer_length / (float) g_sample_rate;
            g_channels = 1;
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:&error]; // to receiver
//            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:&error];
            //            [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker  error:&error];      // this does not seem to route to the speaker; still on receiver
            
            break;
        case kMMAudioSupportWirelessPlayback_W1:
            g_sample_rate = 48000.0f;
            g_output_tap_duration =  (float) g_output_buffer_length / (float) g_sample_rate;
            g_channels = 1;
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions: AVAudioSessionCategoryOptionAllowBluetoothA2DP error:&error];
            break;
        case kMMAudioSupportWirelessPlayback_A2DP:
            g_sample_rate = 44100.0f;
            g_output_tap_duration =  (float) g_output_buffer_length / (float) g_sample_rate;
            g_channels = 1;
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions: AVAudioSessionCategoryOptionAllowBluetoothA2DP error:&error];
            break;
        case kMMAudioSupportWirelessPlayback_HFP:
            // this does not work well over bluetooth
            // may also need to set port to AVAudioSessionPortBluetoothHFP
            g_sample_rate = 8000.0f;
            g_output_buffer_length = 512;
            g_input_buffer_length = 512;
            g_output_tap_duration =  (float) g_output_buffer_length / (float) g_sample_rate;
            g_channels = 1;
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions: AVAudioSessionCategoryOptionAllowBluetooth error:&error];
            break;
            
        case kMMAudioSupportWirelessPlayback_AirPlay:
            // need to test AirPlay 2 devices
            g_sample_rate = 48000.0f;
            g_output_tap_duration =  (float) g_output_buffer_length / (float) g_sample_rate;
            g_channels = 1;
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions: AVAudioSessionCategoryOptionAllowAirPlay error:&error];
            break;
            
        default:
            g_sample_rate = 48000.0f;
            g_channels = 1;
            g_output_tap_duration =  (float) g_output_buffer_length / (float) g_sample_rate;
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
            break;
    }
    
    return error;
}

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _setupPreviewLayer];
    [self _setupAudioEngine];
    
    [AUAudioUnit registerSubclass:[MMAudioGeneratorUnit class] asComponentDescription:[MMAudioGeneratorUnit audioComponentDescription] name:@"Demo: Local InstrumentDemo" version:UINT32_MAX];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self setupCaptureSession];
    
    if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio] != AVAuthorizationStatusAuthorized) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
            NSLog(@"Audio access granted: %@.", granted ? @"YES" : @"NO");
        }];
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.preview.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    
    CGRect bounds = self.preview.bounds;
    self.captureVideoPreviewLayer.bounds = bounds;
    self.captureVideoPreviewLayer.position = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    self.captureVideoPreviewLayer.connection.videoOrientation = [self orientationForConnection];
}

#pragma mark - Setup

- (void)_setupPreviewLayer {
    self.preview = [[UIView alloc] initWithFrame:CGRectZero];
    self.preview.backgroundColor = [UIColor clearColor];
    [self.view insertSubview:self.preview atIndex:0];
}

- (void)_setupAudioEngine {
    self.audioEngine = [[AVAudioEngine alloc] init];
    self.finalMixer = [[AVAudioMixerNode alloc] init];
    self.mixer = [[AVAudioMixerNode alloc] init];
    [self.audioEngine attachNode:self.mixer];
    [self.audioEngine attachNode:self.finalMixer];
}

- (void)setupCaptureSession {
    if(!_session) {
        _session = [[AVCaptureSession alloc] init];
        _session.sessionPreset = AVCaptureSessionPresetHigh;
        
        CGRect bounds = self.preview.layer.bounds;
        _captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
        _captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        _captureVideoPreviewLayer.bounds = bounds;
        _captureVideoPreviewLayer.position = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
        [self.preview.layer addSublayer:_captureVideoPreviewLayer];
        
        AVCaptureDevicePosition devicePosition;
        if([self.class isFrontCameraAvailable]) {
            devicePosition = AVCaptureDevicePositionFront;
        } else {
            devicePosition = AVCaptureDevicePositionBack;
        }
        
        if(devicePosition == AVCaptureDevicePositionUnspecified) {
            self.videoCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        } else {
            self.videoCaptureDevice = [self cameraWithPosition:devicePosition];
        }
        
        NSError *error = nil;
        _videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:_videoCaptureDevice error:&error];
        
        if (!_videoDeviceInput) {
            [self _processError:error];
            return;
        }
        
        if([self.session canAddInput:_videoDeviceInput]) {
            [self.session  addInput:_videoDeviceInput];
            self.captureVideoPreviewLayer.connection.videoOrientation = [self orientationForConnection];
        }
        
        _movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
        [_movieFileOutput setMovieFragmentInterval:kCMTimeInvalid];
        if([self.session canAddOutput:_movieFileOutput]) {
            [self.session addOutput:_movieFileOutput];
        }
    }
    
    if (![self.captureVideoPreviewLayer.connection isEnabled]) {
        [self.captureVideoPreviewLayer.connection setEnabled:YES];
    }
    
    [self.session startRunning];
}

#pragma mark - Utils

- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition) position {
    AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera] mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionFront];
    NSArray *devices = [captureDeviceDiscoverySession devices];
    for (AVCaptureDevice *device in devices) {
        if (device.position == position) return device;
    }
    return nil;
}

- (AVCaptureVideoOrientation)orientationForConnection {
    AVCaptureVideoOrientation videoOrientation = AVCaptureVideoOrientationPortrait;
    
    switch ([UIDevice currentDevice].orientation) {
        case UIDeviceOrientationLandscapeLeft:
            videoOrientation = AVCaptureVideoOrientationLandscapeRight;
            break;
        case UIDeviceOrientationLandscapeRight:
            videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
            break;
        default:
            videoOrientation = AVCaptureVideoOrientationPortrait;
            break;
    }
    
    return videoOrientation;
}

- (void)_processError:(NSError *)error {
    NSLog(@"Error: %@", error.localizedDescription);
    assert(0);
}

#pragma mark - Public Methods

- (void)startRecordingWithVideoOutputUrl:(NSURL *)videoOutputURL
                          audioOutputUrl:(NSURL *)audioOutputURL
                               didRecord:(void (^)(NSURL * _Nonnull, NSError * _Nonnull))completionBlock {
    self.isRecording = YES;
    
    [self _startAudioRecording:audioOutputURL];
    [self _startVideoRecording:videoOutputURL];
    
    self.didRecordCompletionBlock = completionBlock;
}

- (void)stopRecording {
    [self.movieFileOutput stopRecording];
    
    [self _stopAudioRecording];
}

#pragma mark - Video Capture

- (void)_startVideoRecording:(NSURL *)videoOutputFileURL {
    for(AVCaptureConnection *connection in [self.movieFileOutput connections]) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
                if ([connection isVideoOrientationSupported]) {
                    [connection setVideoOrientation:[self orientationForConnection]];
                }
            }
        }
    }
    
    [self.movieFileOutput startRecordingToOutputFileURL:videoOutputFileURL recordingDelegate:self];
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections {
    self.isRecording = YES;
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL
      fromConnections:(NSArray *)connections error:(NSError *)error {
    self.isRecording = NO;
    
    if(self.didRecordCompletionBlock) {
        self.didRecordCompletionBlock(outputFileURL, error);
    }
}

#pragma mark - Audio

- (void)_startAudioRecording:(NSURL *)audioOutputFileURL  {
    NSError* error = nil;
    
    // This will set the category with wireless related AVAudioSessionCategoryOptions if needed.
    error = [self _setAVAudioSessionCategoryPlayAndRecordAccordingToWirelessStatus];
    [[AVAudioSession sharedInstance] setPreferredSampleRate: g_sample_rate error:&error];
/*
 //    [[AVAudioSession sharedInstance] setPreferredInputNumberOfChannels:1 error:&error];
 //    [[AVAudioSession sharedInstance] setPreferredOutputNumberOfChannels: 1 error:&error];
 //    [[AVAudioSession sharedInstance] setPreferredIOBufferDuration: (1.00 * ((float) g_input_buffer_length/(float) g_sample_rate) ) error:&error];

 NSArray *availableInputs = [[AVAudioSession sharedInstance] availableInputs];
    AVAudioSessionPortDescription *port = [availableInputs firstObject];  //built in mic
    NSLog( @"[port portName]==%@", [port portName] );

    NSArray<AVAudioSessionDataSourceDescription*> *availableInputDataSources = [[AVAudioSession sharedInstance] inputDataSources];
    for( NSInteger i = 0; i < [availableInputDataSources count]; i++ )  {
        NSLog( @"input data source [%ld]: [%@][%@]", i,
              [[availableInputDataSources objectAtIndex:i] dataSourceID],
              [[availableInputDataSources objectAtIndex:i] dataSourceName]  );
    }

    NSArray<AVAudioSessionDataSourceDescription*> *availableOutputDataSources = [[AVAudioSession sharedInstance] outputDataSources];
    for( NSInteger i = 0; i < [availableOutputDataSources count]; i++ )  {
        NSLog( @"output data source [%ld]: [%@][%@]", i,
              [[availableOutputDataSources objectAtIndex:i] dataSourceID],
              [[availableOutputDataSources objectAtIndex:i] dataSourceName]  );
    }
*/
    
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    if (error != nil) {
        [self _processError:error];
    }
    
    __weak typeof(self) weakSelf = self;
    g_max_off_sync = 0.0f;
    g_output_counter = 0;
    [AVAudioUnit instantiateWithComponentDescription:[MMProcessingAudioUnit audioComponentDescription]
                                             options:0
                                   completionHandler:^(__kindof AVAudioUnit *_Nullable processingAudioUnit, NSError *_Nullable error) {
        if (error != nil) {
           [weakSelf _processError:error];
        }

        g_output_counter = 0;
        [AVAudioUnit instantiateWithComponentDescription:[MMAudioGeneratorUnit audioComponentDescription]
                                                options:0
                                      completionHandler:^(__kindof AVAudioUnit *_Nullable generatorAudioUnit, NSError *_Nullable error) {
            if (error != nil) {
                [weakSelf _processError:error];
            }

            [weakSelf _setupAudioEngineNodesWithGeneratorAudioUnit:generatorAudioUnit
                                                processingAudioUnit:processingAudioUnit
                                              recordingAudioFileURL:audioOutputFileURL];

        }];
    }];
}

- (void)_setupAudioEngineNodesWithGeneratorAudioUnit:(__kindof AVAudioUnit *_Nullable)generatorAudioUnit
           processingAudioUnit:(__kindof AVAudioUnit *_Nullable)processingAudioUnit
            recordingAudioFileURL:(NSURL *)audioOutputFileURL{
    NSError* error = nil;
    if (!generatorAudioUnit) {
        [self _processError:error];
        return;
    }
    
    [self.audioEngine attachNode:processingAudioUnit];
    [self.audioEngine attachNode:generatorAudioUnit];
    
    self.audioFile = [[AVAudioFile alloc] initForReading:self.audioFileURL error:&error];
    if (error != nil) {
        [self _processError:error];
        return;
    }
    
    // AVAudioPCMFormatFloat32 is standard.
    AVAudioFormat* format = [[AVAudioFormat alloc] initStandardFormatWithSampleRate: g_sample_rate channels: g_channels];
    
    [self.audioEngine connect:self.audioEngine.inputNode to:self.mixer format:format];
    [self.audioEngine connect:generatorAudioUnit to:self.mixer format:format];
    [self.audioEngine connect:self.mixer to:self.finalMixer format:format];
    [self.audioEngine connect:processingAudioUnit to:self.finalMixer format:format];
    [self.audioEngine connect:self.finalMixer to:[self.audioEngine mainMixerNode] format:format];
    
    __block Float64 phase = 0;
    MMAudioGeneratorUnit *sampleUnit = (MMAudioGeneratorUnit *)generatorAudioUnit.AUAudioUnit;
    [sampleUnit setRenderBlock:^(AVAudioPCMBuffer *buffer) {
        AVAudioFormat *format = buffer.format;
        const Float64 currentPhase = phase;
        const Float64 phasePerFrame = g_sine_frequency / format.sampleRate * 2.0 * M_PI;
        for (UInt32 ch_idx = 0; ch_idx < format.channelCount; ++ch_idx) {
            Float32 *ptr = buffer.floatChannelData[ch_idx];
            phase = fill_sine(ptr, buffer.frameLength, currentPhase, phasePerFrame);
        }
        
        float off_sync = g_input_counter * ( (float) g_input_buffer_length / (float) g_sample_rate ) - g_output_counter * g_output_tap_duration;
        if( g_max_off_sync < fabsf( off_sync ) )  {
            g_max_off_sync = off_sync;
        }
        NSLog( @"g_input_counter==%u; g_output_counter==%u; diff==(%.1f)ms, ratio==[%.4f], max_off_sync=(%.1f)ms",
              g_input_counter,
              g_output_counter,
              off_sync * 1000.0,
              g_input_counter * ( (float) g_input_buffer_length / (float) g_sample_rate ) / ( g_output_counter * g_output_tap_duration ),
              g_max_off_sync * 1000.0
              );
        
    }];
    
    [self _setupAudioRecording:audioOutputFileURL format:format];
    
    [self.audioEngine startAndReturnError:&error];
    if (error != nil) {
        [self _processError:error];
        return;
    }
}

- (void)_setupAudioRecording:(NSURL *)audioOutputFileURL format:(AVAudioFormat *)format {
    ExtAudioFileCreateWithURL(CFBridgingRetain(audioOutputFileURL),
                              kAudioFileWAVEType,
                              format.streamDescription,
                              nil,
                              kAudioFileFlags_EraseFile,
                              &self->outref);
    
    [self.mixer installTapOnBus:0
                     bufferSize: format.sampleRate*g_output_tap_duration
                         format:format
                          block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
                              AVAudioBuffer* audioBuffer = buffer;
                              ExtAudioFileWrite(self->outref, buffer.frameLength, audioBuffer.audioBufferList);
                          }];
}

- (void)_stopAudioRecording {
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    
    [self.audioFilePlayer stop];
    [self.audioEngine stop];
    [self.mixer removeTapOnBus:0];
    ExtAudioFileDispose(outref);
}

#pragma mark - Class Methods

+ (void)requestCameraPermission:(void (^)(BOOL granted))completionBlock {
    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(completionBlock) {
                    completionBlock(granted);
                }
            });
        }];
    } else {
        completionBlock(YES);
    }
}

+ (void)requestMicrophonePermission:(void (^)(BOOL granted))completionBlock {
    if([[AVAudioSession sharedInstance] respondsToSelector:@selector(requestRecordPermission:)]) {
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(completionBlock) {
                    completionBlock(granted);
                }
            });
        }];
    }
}

+ (BOOL)isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

+ (BOOL)isRearCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

@end
