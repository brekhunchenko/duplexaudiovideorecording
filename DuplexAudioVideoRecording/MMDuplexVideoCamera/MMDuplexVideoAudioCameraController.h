//
//  MMDuplexVideoAudioCameraController.h
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 10/29/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MMDuplexVideoAudioCameraController : UIViewController

@property (nonatomic, strong) NSURL* audioFileURL;

@property (nonatomic, assign, readonly) BOOL isRecording;
- (void)startRecordingWithVideoOutputUrl:(NSURL *)videoOutputURL
                          audioOutputUrl:(NSURL *)audioOutputURL
                          didRecord:(void (^)(NSURL *outputFileUrl, NSError *error))completionBlock;
- (void)stopRecording;

@end

NS_ASSUME_NONNULL_END
