//
//  MMAudioUnit.h
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 11/6/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MMProcessingAudioUnit : AUAudioUnit

+ (AudioComponentDescription)audioComponentDescription;

@end

NS_ASSUME_NONNULL_END
