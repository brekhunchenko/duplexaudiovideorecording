//
//  MMAudioUnit.m
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 11/6/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMProcessingAudioUnit.h"
#import <AVFoundation/AVFoundation.h>
#import "extern.h"

@interface MMProcessingAudioUnit()

@property AUAudioUnitBusArray *inputBusArray;
@property AUAudioUnitBusArray *outputBusArray;

@property (copy) AUInternalRenderBlock internalBlock;

@end

@implementation MMProcessingAudioUnit

#pragma mark - Initializers

+ (void)initialize {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self registerSubclass:[MMProcessingAudioUnit class]
        asComponentDescription:[self audioComponentDescription]
                          name:@"ProccessingAudioUnitName"
                       version:UINT32_MAX];
    });
}

- (nullable instancetype)initWithComponentDescription:(AudioComponentDescription)componentDescription
                                              options:(AudioComponentInstantiationOptions)options
                                                error:(NSError **)outError {
    self = [super initWithComponentDescription:componentDescription options:options error:outError];
    if (self) {
        self.internalBlock = ^AUAudioUnitStatus(AudioUnitRenderActionFlags *actionFlags, const AudioTimeStamp *timeStamp,
                             AVAudioFrameCount frameCount, NSInteger outputBusNumber, AudioBufferList *outputData,
                             const AURenderEvent *realtimeEventListHead, AURenderPullInputBlock pullInputBlock) {
            
//            float *processingData  = (float*)outputData->mBuffers[0].mData;
//            NSLog(@"Audio data chunk processing: %f", processingData[12]);
            g_input_counter++;
            
            return noErr;
        };
        
        AVAudioFormat *format = [[AVAudioFormat alloc] initStandardFormatWithSampleRate: g_sample_rate channels: g_channels];
        AUAudioUnitBus *outputBus = [[AUAudioUnitBus alloc] initWithFormat:format error:nil];
        self.outputBusArray = [[AUAudioUnitBusArray alloc] initWithAudioUnit:self busType:AUAudioUnitBusTypeOutput busses:@[outputBus]];
        
        self.maximumFramesToRender = g_input_buffer_length; // not sure if this makes a difference
    }
    return self;
}

#pragma mark - Overridden

- (AUInternalRenderBlock)internalRenderBlock {
    return self.internalBlock;
}

- (AUAudioUnitBusArray *)outputBusses {
    return self.outputBusArray;
}

- (BOOL)shouldChangeToFormat:(AVAudioFormat *)format forBus:(AUAudioUnitBus *)bus {
    return YES;
}

- (BOOL)allocateRenderResourcesAndReturnError:(NSError *_Nullable __autoreleasing *)outError {
    BOOL result = [super allocateRenderResourcesAndReturnError:outError];
    return result;
}

- (void)deallocateRenderResources {
    [super deallocateRenderResources];
}

#pragma mark - Class Methods

+ (AudioComponentDescription)audioComponentDescription {
    AudioComponentDescription audioComponentDescriptino = {
        .componentType = kAudioUnitType_Generator,
//        .componentSubType = kAudioUnitSubType_RemoteIO, // 'roic', // 'smpl',
//        .componentManufacturer = kAudioUnitManufacturer_Apple,
        .componentSubType = 'smpl',
        .componentManufacturer = 'Gnrt',
        .componentFlags = 0,
        .componentFlagsMask = 0,
    };
    return audioComponentDescriptino;
}


@end
