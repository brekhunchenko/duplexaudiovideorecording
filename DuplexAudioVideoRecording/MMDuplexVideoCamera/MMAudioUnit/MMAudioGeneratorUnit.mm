//
//  MMAudioGeneratorUnit.m
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 11/15/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMAudioGeneratorUnit.h"
#import "extern.h"

@interface AudioUnitGeneratorSampleKernel : NSObject

@property (nullable) AVAudioPCMBuffer *outputBuffer;
@property (copy) void (^renderBlock)(AVAudioPCMBuffer *buffer);

@end

@implementation AudioUnitGeneratorSampleKernel

- (void)setupBufferWithFormat:(AVAudioFormat *)format frames:(NSUInteger)frames {
    self.outputBuffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:format frameCapacity:(AVAudioFrameCount)frames];
}

- (void)disposeBuffer {
    self.outputBuffer = nil;
}

@end

@interface MMAudioGeneratorUnit ()

@property AudioUnitGeneratorSampleKernel *kernel;
@property (copy) AUInternalRenderBlock internalBlock;

@property AUAudioUnitBusArray *inputBusArray;
@property AUAudioUnitBusArray *outputBusArray;

@end

@implementation MMAudioGeneratorUnit

+ (void)initialize
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self registerSubclass:[MMAudioGeneratorUnit class]
        asComponentDescription:[self audioComponentDescription]
                          name:@"AudioGeneratorUnit"
                       version:UINT32_MAX];
    });
}

- (nullable instancetype)initWithComponentDescription:(AudioComponentDescription)componentDescription
                                              options:(AudioComponentInstantiationOptions)options
                                                error:(NSError **)outError
{
    self = [super initWithComponentDescription:componentDescription options:options error:outError];
    if (self) {
        AudioUnitGeneratorSampleKernel *kernel = [[AudioUnitGeneratorSampleKernel alloc] init];
        self.kernel = kernel;
        
        self.internalBlock = ^AUAudioUnitStatus(AudioUnitRenderActionFlags *actionFlags, const AudioTimeStamp *timeStamp,
                                                AVAudioFrameCount frameCount, NSInteger outputBusNumber, AudioBufferList *outputData,
                                                const AURenderEvent *realtimeEventListHead, AURenderPullInputBlock pullInputBlock) {
            AVAudioPCMBuffer *buffer = kernel.outputBuffer;
            buffer.frameLength = frameCount;
            
            void (^renderBlock)(AVAudioPCMBuffer *buffer) = kernel.renderBlock;
            
            if (renderBlock) {
                renderBlock(buffer);
            }
            
            for (UInt32 i = 0; i < outputData->mNumberBuffers; ++i) {
                void *outData = outputData->mBuffers[i].mData;
                void *inData = buffer.mutableAudioBufferList->mBuffers[i].mData;
                if (outData == NULL) {
                    outputData->mBuffers[i].mData = inData;
                } else if (outData != inData) {
                    memcpy(outData, inData, outputData->mBuffers[i].mDataByteSize);
                }
            }

            g_output_counter++;
            
            return noErr;
        };
        
        AVAudioFormat *format = [[AVAudioFormat alloc] initStandardFormatWithSampleRate: g_sample_rate channels: g_channels];
        NSError* error = nil;
        AUAudioUnitBus *outputBus = [[AUAudioUnitBus alloc] initWithFormat:format error:&error];
        if (error != nil) {
            NSLog(@"Error.");
        }
        self.outputBusArray = [[AUAudioUnitBusArray alloc] initWithAudioUnit:self busType:AUAudioUnitBusTypeOutput busses:@[outputBus]];
        
        self.maximumFramesToRender = g_output_buffer_length;    // not sure if this makes a difference
    }
    return self;
}

- (AUInternalRenderBlock)internalRenderBlock {
    return self.internalBlock;
}

- (void)setRenderBlock:(void (^)(AVAudioPCMBuffer *))renderBlock {
    self.kernel.renderBlock = renderBlock;
}

- (AUAudioUnitBusArray *)outputBusses {
    return self.outputBusArray;
}

- (BOOL)shouldChangeToFormat:(AVAudioFormat *)format forBus:(AUAudioUnitBus *)bus {
    return YES;
}

- (BOOL)allocateRenderResourcesAndReturnError:(NSError *_Nullable __autoreleasing *)outError {
    BOOL result = [super allocateRenderResourcesAndReturnError:outError];
    
    if (result) {
        AUAudioUnitBus *bus = self.outputBusArray[0];
        [self.kernel setupBufferWithFormat:bus.format frames:self.maximumFramesToRender];
    }
    
    return result;
}

- (void)deallocateRenderResources {
    [self.kernel disposeBuffer];
    
    [super deallocateRenderResources];
}

#pragma mark - Class Methods

+ (AudioComponentDescription)audioComponentDescription {
    AudioComponentDescription acd = {
        .componentType = kAudioUnitType_Generator,
//        .componentSubType = kAudioUnitSubType_RemoteIO, // 'roic', // 'smpl',
//        .componentManufacturer = kAudioUnitManufacturer_Apple, // 'appl',// 'Gnrt',
        .componentSubType = 'smpl',
        .componentManufacturer = 'Prcs',        // why 'Prcs' works??
        .componentFlags = 0,
        .componentFlagsMask = 0,
    };
    return acd;
}

@end
