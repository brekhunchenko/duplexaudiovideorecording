//
//  MMAudioGeneratorUnit.h
//  DuplexAudioVideoRecording
//
//  Created by Yaroslav Brekhunchenko on 11/15/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MMAudioGeneratorUnit : AUAudioUnit

+ (AudioComponentDescription)audioComponentDescription;

- (void)setRenderBlock:(void (^)(AVAudioPCMBuffer *))renderBlock;

@end

NS_ASSUME_NONNULL_END
